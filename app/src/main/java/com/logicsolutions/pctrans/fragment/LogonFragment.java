package com.logicsolutions.pctrans.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.AdapterView;
import android.widget.ListView;

import com.logicsolutions.pctrans.R;

import java.util.ArrayList;

import com.logicsolutions.pctrans.adapter.StatusAdapter;
import com.logicsolutions.pctrans.adapter.VehicleAdapter;
import com.logicsolutions.pctrans.object.Status;
import com.logicsolutions.pctrans.object.Vehicle;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LogonFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LogonFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LogonFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LogonFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LogonFragment newInstance() {
        LogonFragment fragment = new LogonFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public LogonFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_logon, container, false);

        final ListView listview = (ListView) view.findViewById(R.id.lv_vehicles_logon);
        Vehicle[] values = new Vehicle[]
                {
                        new Vehicle("B01", "2014-01-03"),
                        new Vehicle("B02", "2014-02-03"),
                        new Vehicle("B03", "2014-03-03"),
                        new Vehicle("B04", "2014-04-03"),
                        new Vehicle("B05", "2014-05-03"),
                        new Vehicle("B06", "2014-06-03"),
                        new Vehicle("B07", "2014-07-03"),
                        new Vehicle("B08", "2014-08-03"),
                        new Vehicle("B09", "2014-09-03"),
                        new Vehicle("B10", "2014-10-03"),
                        new Vehicle("B11", "2014-11-03"),
                        new Vehicle("B12", "2014-12-03"),
                        new Vehicle("B13", "2015-01-03"),
                        new Vehicle("B01", "2014-01-03"),
                        new Vehicle("B02", "2014-02-03"),
                        new Vehicle("B03", "2014-03-03"),
                        new Vehicle("B04", "2014-04-03"),
                        new Vehicle("B05", "2014-05-03"),
                        new Vehicle("B06", "2014-06-03"),
                        new Vehicle("B07", "2014-07-03"),
                        new Vehicle("B08", "2014-08-03"),
                        new Vehicle("B09", "2014-09-03"),
                        new Vehicle("B10", "2014-10-03"),
                        new Vehicle("B11", "2014-11-03"),
                        new Vehicle("B12", "2014-12-03"),
                        new Vehicle("B13", "2015-01-03"),
                        new Vehicle("B01", "2014-01-03"),
                        new Vehicle("B02", "2014-02-03"),
                        new Vehicle("B03", "2014-03-03"),
                        new Vehicle("B04", "2014-04-03"),
                        new Vehicle("B05", "2014-05-03"),
                        new Vehicle("B06", "2014-06-03"),
                        new Vehicle("B07", "2014-07-03"),
                        new Vehicle("B08", "2014-08-03"),
                        new Vehicle("B09", "2014-09-03"),
                        new Vehicle("B10", "2014-10-03"),
                        new Vehicle("B11", "2014-11-03"),
                        new Vehicle("B12", "2014-12-03"),
                        new Vehicle("B13", "2015-01-03"),
                };

        final ArrayList<Vehicle> list = new ArrayList<Vehicle>();
        for (int i = 0; i < values.length; ++i) {
            list.add(values[i]);
        }
        final VehicleAdapter adapter = new VehicleAdapter(getActivity(), list);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
//                final String item = (String) parent.getItemAtPosition(position);
//                view.animate().setDuration(2000).alpha(0)
//                        .withEndAction(new Runnable() {
//                            @Override
//                            public void run() {
//                                list.remove(item);
//                                adapter.notifyDataSetChanged();
//                                view.setAlpha(1);
//                            }
//                        });
            }

        });

        final ListView listview2 = (ListView) view.findViewById(R.id.lv_detail_logon);
        Status[] status = new Status[]
                {
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns"),
                        new Status("Out of Service", "03:30p", "Clinton Transit (office/garage) 304 Brush St, Saint Johns")

                };

        final ArrayList<Status> list2 = new ArrayList<Status>();
        for (int i = 0; i < status.length; ++i) {
            list2.add(status[i]);
        }
        final StatusAdapter adapter2 = new StatusAdapter(getActivity(), list2);
        listview2.setAdapter(adapter2);

        listview2.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
//                final String item = (String) parent.getItemAtPosition(position);
//                view.animate().setDuration(2000).alpha(0)
//                        .withEndAction(new Runnable() {
//                            @Override
//                            public void run() {
//                                list.remove(item);
//                                adapter.notifyDataSetChanged();
//                                view.setAlpha(1);
//                            }
//                        });
            }

        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
