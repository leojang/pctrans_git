package com.logicsolutions.pctrans.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.logicsolutions.pctrans.R;
import java.util.ArrayList;
import com.logicsolutions.pctrans.object.Vehicle;

/**
 * Created by Rplus on 2/2/2015.
 */
public class VehicleAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<Vehicle> objects;

    private class ViewHolder {
        TextView vehicle;
        TextView date;
    }

    public VehicleAdapter(Context context, ArrayList<Vehicle> objects) {
        inflater = LayoutInflater.from(context);
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    public Vehicle getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.listview_item_vehicle, null);
            holder.vehicle = (TextView) convertView.findViewById(R.id.tv_vehicle);
            holder.date = (TextView) convertView.findViewById(R.id.tv_available_date);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.vehicle.setText(objects.get(position).getNumber());
        holder.date.setText(objects.get(position).getDate());
        return convertView;
    }
}
