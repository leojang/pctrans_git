package com.logicsolutions.pctrans.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.logicsolutions.pctrans.R;

import java.util.ArrayList;

import com.logicsolutions.pctrans.object.Status;

/**
 * Created by Rplus on 2/2/2015.
 */
public class StatusAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<Status> objects;

    private class ViewHolder {
        TextView state;
        TextView time;
        TextView detail;
    }

    public StatusAdapter(Context context, ArrayList<Status> objects) {
        inflater = LayoutInflater.from(context);
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    public Status getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.listview_item_status, null);
            holder.state = (TextView) convertView.findViewById(R.id.tv_state);
            holder.time = (TextView) convertView.findViewById(R.id.tv_time);
            holder.detail = (TextView) convertView.findViewById(R.id.tv_detail);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.state.setText(objects.get(position).getState());
        holder.time.setText(objects.get(position).getTime());
        holder.detail.setText(objects.get(position).getDetail());
        return convertView;
    }
}
