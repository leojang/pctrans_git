package com.logicsolutions.pctrans.object;

/**
 * Created by Rplus on 2/2/2015.
 */
public class Vehicle {

    private String number;
    private String date;

    public Vehicle(String number, String date) {
        this.number = number;
        this.date = date;
    }

    public String getNumber() {
        return number;
    }

    public String getDate() {
        return date;
    }

}
