package com.logicsolutions.pctrans.object;

/**
 * Created by Rplus on 2/2/2015.
 */
public class Status {

    private String state;
    private String time;
    private String detail;

    public Status(String state, String time, String detail) {
        this.state = state;
        this.time = time;
        this.detail = detail;
    }

    public String getState() {
        return state;
    }

    public String getTime() {
        return time;
    }

    public String getDetail() {
        return detail;
    }

}
